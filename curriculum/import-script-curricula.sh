echo "ECURRICULA CURRICULUM BACKUP ASSISTANT 1.0"

    cd "curriculum"
	echo "Importing Curriculum"
    
    echo "User's Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c USERS_TABLE --file USERS_TABLE.json
    
    echo "D3 Course Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c D3_COURSE_TABLE --file D3_COURSE_TABLE.json
    
    echo "D3 Future Prospects Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c D3_FUTURE_PROSPECTS --file D3_FUTURE_PROSPECTS.json
    
    echo "Department Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c DEPARTMENT_TABLE --file DEPARTMENT_TABLE.json
    
   