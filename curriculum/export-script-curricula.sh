echo "ECURRICULA COURSE BACKUP ASSISTANT 1.0"

    mkdir "curriculum"
    cd "curriculum"

	echo "Backing Up Curriculum"
    
    echo "User's Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o USERS_TABLE.json -c USERS_TABLE -q "{'role':'D'}"
    
    echo "D3 Course Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o D3_COURSE_TABLE.json -c D3_COURSE_TABLE
    
    echo "D3 Future Prospects Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o D3_FUTURE_PROSPECTS.json -c D3_FUTURE_PROSPECTS
    
    echo "Department Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o DEPARTMENT_TABLE.json -c DEPARTMENT_TABLE 
    