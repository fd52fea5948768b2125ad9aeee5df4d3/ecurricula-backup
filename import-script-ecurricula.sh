echo "ECURRICULA COURSE BACKUP ASSISTANT 1.0"
if [ -z "$1" ] 
then 
	echo "Provide a Course Code!!"
else
    cd "$1"
	echo "Importing Course: $1"
    
    echo "User's Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c USERS_TABLE --file USERS_TABLE.json
    
    echo "Unit's Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c UNITS_TABLE --file UNITS_TABLE.json
    
    echo "Short Question Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c SHORT_QUESTION_TABLE --file SHORT_QUESTION_TABLE.json
    
    echo "Long Question Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c LONG_QUESTION_TABLE --file LONG_QUESTION_TABLE.json
    
    echo "Quiz Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c QUIZ_TABLE --file QUIZ_TABLE.json
    
    echo "Learning Plan Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c LEARNING_PLAN_TABLE --file LEARNING_PLAN_TABLE.json
    
    echo "Course Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c COURSE_TABLE --file COURSE_TABLE.json
    
    echo "Session Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c "$1_SESSION_TABLE" --file SESSION_TABLE.json
    
    echo "Coordinator Table"
    mongoimport -d ECURRICULA_DB -u ulcuser -p ulcuserdbpassword --upsert -c "$1_COORDINATOR_TABLE" --file COORDINATOR_TABLE.json
    
    
fi