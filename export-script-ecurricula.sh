echo "ECURRICULA COURSE BACKUP ASSISTANT 1.0"
if [ -z "$1" ] 
then 
	echo "Provide A Course Code"
else
    mkdir "$1"
    cd "$1"

	echo "Backing Up for Course: $1"
    
    echo "User's Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o USERS_TABLE.json -c USERS_TABLE -q "{_id:'coordinator_$1'}"
    
    echo "Unit's Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o UNITS_TABLE.json -c UNITS_TABLE -q "{courseCode:'$1'}"
    
    echo "Short Question Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o SHORT_QUESTION_TABLE.json -c SHORT_QUESTION_TABLE -q "{unitID:/^$1/}"
    
    echo "Long Question Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o LONG_QUESTION_TABLE.json -c LONG_QUESTION_TABLE -q "{unitID:/^$1/}"
    
    echo "Quiz Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o QUIZ_TABLE.json -c QUIZ_TABLE -q "{unitID:/^$1/}"
    
    echo "Learning Plan Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o LEARNING_PLAN_TABLE.json -c LEARNING_PLAN_TABLE -q "{courseID:'$1'}"
    
    echo "Course Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o COURSE_TABLE.json -c COURSE_TABLE -q "{_id:'$1'}"
    
    echo "Session Table"
    mongoexport -u ulcuser -p ulcuserdbpassword -d ECURRICULA_DB -o SESSION_TABLE.json -c "$1"_SESSION_TABLE
    
    echo "Coordinator Table"
    echo "{\"_id\":\"coordinator\",\"type\":\"sample\"}" > COORDINATOR_TABLE.json
    
    
fi