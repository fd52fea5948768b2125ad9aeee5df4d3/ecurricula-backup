echo "ECURRICULA COMPLETE BACKUP ASSISTANT 1.0"
if [ -z "$1" ] 
then 
	echo "Provide A FILE NAME"
else
    echo "Backing Up...."
    mongodump --host localhost --port 27017 --db ECURRICULA_DB --username ulcuser --password ulcuserdbpassword --out /home/bitnami/backup/"$1"
    
    
    echo "Backup Process Completed"
fi