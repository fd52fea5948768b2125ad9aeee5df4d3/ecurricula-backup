echo "ECURRICULA COURSE BACKUP ASSISTANT 1.0"
if [ -z "$1" ] 
then 
	echo "Provide a Folder Name!!"
else    
    echo "Importing..."
    
    mongorestore --host localhost --port 27017 --db ECURRICULA_DB --username ulcuser --password ulcuserdbpassword /home/bitnami/backup/"$1"/ECURRICULA_DB
    
fi